[![Yii UI](https://www.yii-ui.com/logos/logo-yii-ui-readme.jpg)](https://www.yii-ui.com/) Yii UI - Yii2 Overlay Scrollbars - Asset Bundle
================================================

[![Latest Stable Version](https://poser.pugx.org/yii-ui/yii2-overlay-scrollbars-asset-bundle/version)](https://packagist.org/packages/yii-ui/yii2-overlay-scrollbars-asset-bundle)
[![Total Downloads](https://poser.pugx.org/yii-ui/yii2-overlay-scrollbars-asset-bundle/downloads)](https://packagist.org/packages/yii-ui/yii2-overlay-scrollbars-asset-bundle)
[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat)](http://www.yiiframework.com/)
[![License](https://poser.pugx.org/yii-ui/yii2-overlay-scrollbars-asset-bundle/license)](https://packagist.org/packages/yii-ui/yii2-overlay-scrollbars-asset-bundle)


This is an [Yii framework 2.0](http://www.yiiframework.com) asset bundle of the [KingSora OverlayScrollbars](https://github.com/KingSora/OverlayScrollbars) plugin.

Installation
------------

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run
```
php composer.phar require yii-ui/yii2-overlay-scrollbars-asset-bundle
```
or add
```
"yii-ui/yii2-overlay-scrollbars-asset-bundle": "^1.0"
```
to the require section of your `composer.json` file.

Usage
-----

in your layout file (ex. views/layouts/main.php):
```php
\yiiui\yii2overlayscrollbars\assets\OverlayScrollbarsAsset::register($this);
```

or as dependency in your app asset bundle:
```php
namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $sourcePath = '@app/assets';

    public $js = [
        'js/main.js',
    ];

    public $css = [
        'css/main.scss',
    ];

    public $depends = [
        'yiiui\yii2overlayscrollbars\assets\OverlayScrollbarsAsset'
    ];
}
```

See https://www.yii-ui.com/packages/yii2-overlay-scrollbars-asset-bundle for more infos.
For plugin configuration see [KingSora](https://github.com/KingSora) OverlayScrollbars [Documentation](https://kingsora.github.io/OverlayScrollbars/#!documentation).

Documentation
------------

Documentation can be found at https://www.yii-ui.com/packages/yii2-overlay-scrollbars-asset-bundle/docs.

License
-------

**yii2-overlay-scrollbars-asset-bundle** is released under the MIT License. See the [LICENSE](LICENSE) for details.
