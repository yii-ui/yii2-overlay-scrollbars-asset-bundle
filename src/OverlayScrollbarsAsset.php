<?php
/**
 * @author Christoph Möke <christophmoeke@gmail.com>
 * @copyright Copyright (c) 2019 Yii UI
 * @license https://www.yii-ui.com/packages/yii2-overlay-scrollbars-asset-bundle/license MIT
 * @link https://www.yii-ui.com/packages/yii2-overlay-scrollbars-asset-bundle
 * @see https://www.yii-ui.com/packages/yii2-overlay-scrollbars-asset-bundle/docs Documentation of yii2-overlay-scrollbars-asset-bundle
 * @since File available since Release 1.0.0
 */

namespace yiiui\yii2overlayscrollbars\assets;

use yii\web\AssetBundle;

/**
 * Class OverlayScrollbarsAsset
 * @package yiiui\yii2overlayscrollbars\assets
 */
class OverlayScrollbarsAsset extends AssetBundle
{
    public $sourcePath = '@npm/overlayscrollbars';

    public $js = [
        'js/OverlayScrollbars.min.js'
    ];

    public $css = [
        'css/OverlayScrollbars.min.css'
    ];
}
